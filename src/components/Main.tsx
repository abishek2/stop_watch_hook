import React from "react";
import "./Main.css";
import { AiOutlineClockCircle } from 'react-icons/ai';

let timerId:NodeJS.Timeout;
const Main: React.FC = () => {
  
  const [isStarted,setIsStarted]=React.useState(false);
  /*this state is to disable start button when timer is already started*/
  const [timerVal,setTimerVal]=React.useState([0,0,0]);
  let timerCount:number=0;

  const startTimer=()=>{
    setIsStarted(true);
    timerId=setInterval(() => { timerCount++; formatTime(); },1000)
  }

  const resetTimer=()=>{
    timerCount=0;
    clearTimeout(timerId);
    setTimerVal([0,0,0,]);//calling state function
    setIsStarted(false);
  }
  const formatTime=()=>{
    let tempTimerVal = timerCount;
    let newTime: number[] = [0, 0, 0];
    if (tempTimerVal > 86400) {
      tempTimerVal = 0;
    }
    if (tempTimerVal / 3600 >= 1) {
      newTime[0] = Math.floor(tempTimerVal / 3600);
      tempTimerVal %= 3600;
    }
    if (tempTimerVal / 60 >= 1) {
      newTime[1] = Math.floor(tempTimerVal / 60);
      tempTimerVal %= 60;
    }
    newTime[2] = tempTimerVal;
    setTimerVal(newTime);
  }
  const getTwoDigit=(num:number)=>{
    return `0${num}`;
  }
  const getTime=(tempArr:number[])=>{
    let time:string;
    /*this part take array element and format them like two digit number separated by colon ex. 00:00:00*/
    console.log(tempArr)
    time=`${tempArr[0]>=10?tempArr[0]:getTwoDigit(tempArr[0])} : ${tempArr[1]>=10?tempArr[1]:getTwoDigit(tempArr[1])} : ${tempArr[2]>=10?tempArr[2]:getTwoDigit(tempArr[2])}`;
    return time;
  }

  return (<main className="mainBox">
    <div className="backgroundBox">
      <div className="contentBox">
        <section className="titleSection">
          <div className="title">React Stopwatch </div>
          <AiOutlineClockCircle />
        </section>
        <div className="stopWatchBox">
          <div className="time">{getTime(timerVal)}</div>
          <div className="buttons">
            <button onClick={isStarted?()=>{alert("Hey, timer is already started")}:startTimer} className="button">Start</button>
            <button onClick={resetTimer} className="button">Reset</button>
          </div>
        </div>
      </div>
    </div>
  </main>);
}

export default Main;